import requests
import botogram
import datetime
from dateutil import tz
import fastf1
from tabulate import tabulate
from src import config
from plot_speed_on_track import plotting

bot = botogram.create(config.token_tg)
bot.owner = "@morozova_nika"


def convert_time(strr):
    strr = strr.replace("T", "")
    a = strr.split(":")
    h = str(int(a[0]) + 1)
    return h + ":" + a[1] + ":" + a[2]


def control_year(year):
    url = "http://ergast.com/api/f1/{}".format(year)

    r = requests.get(url)

    if r.status_code == 200:
        return year
    else:
        return "current"


def diffdate(data, f):
    datetime_format = '%d/%m/%Y %H:%M:%S'
    now = datetime.datetime.now()
    date1 = data + " " + f
    date2 = now.strftime("%d/%m/%Y %H:%M:%S")
    diff = (datetime.datetime.strptime(date1, datetime_format) -
            datetime.datetime.strptime(date2, datetime_format))

    return diff


def convert_data(strr):
    strr = strr.split("-")
    return strr[2] + "/" + strr[1] + "/" + strr[0]


@bot.callback("racelast")
def racelast(message):
    url = "http://ergast.com/api/f1/current/last/results.json"

    r = requests.get(url)

    data = r.json()['MRData']['RaceTable']['Races'][0]['Results']
    btns = botogram.Buttons()
    btns[0].callback("Qualify", "qualylast")
    btns[1].callback("Go back", "menulast")
    text = "DRIVER | GAP\n\n"
    for driver in data:
        try:
            text += driver['Driver']['code'] + " | " + \
                    driver['Time']['time'] + "\n"
        except KeyError:
            try:
                text += driver['Driver']['code'] + " | " + \
                        driver['status'] + "\n"
            except Exception:
                pass
    message.edit(text, attach=btns)


@bot.callback("qualylast")
def qualyslast(message):
    url = "http://ergast.com/api/f1/current/last/qualifying.json"

    r = requests.get(url)

    data = r.json()['MRData']['RaceTable']['Races'][0]['QualifyingResults']

    btns = botogram.Buttons()
    btns[0].callback("Race", "racelast")
    btns[1].callback("Go back", "menulast")
    text = "DRIVER | Time\n\n"
    for driver in data:
        try:
            if driver['Q3'] != "":
                text += driver['Driver']['code'] + " | " + \
                        driver['Q3'] + "\n"
            else:
                text += driver['Driver']['code'] + " | DNF\n"
        except KeyError:
            try:
                if driver['Q2'] != "":
                    text += driver['Driver']['code'] + " | " + \
                            driver['Q2'] + "\n"
                else:
                    text += driver['Driver']['code'] + " | DNF\n"
            except KeyError:
                if driver['Q1'] != "":
                    text += driver['Driver']['code'] + " | " + \
                            driver['Q1'] + "\n"
                else:
                    text += driver['Driver']['code'] + " | DNF\n"
    message.edit(text, attach=btns)



@bot.command("last")
def last_command(chat):
    """Show the results of the last f1 race and qualifying."""
    url = "https://api.openf1.org/v1/meetings?year=" + str(datetime.datetime.now().year)

    r = requests.get(url)

    data = r.json()
    r = len(data)
    data = data[len(data) - 1]

    chat.send((
                  "*{}*\n*Round: *{}\n*Date: *{}\n"
              ).format(data['meeting_official_name'], r,
                       datetime.datetime.strptime(data['date_start'].replace("T", " ")[:-6], \
                       '%Y-%m-%d %H:%M:%S').replace(tzinfo=tz.gettz('UTC')).astimezone(tz.tzlocal())),
              syntax="markdown")


@bot.command("next")
def next_command(chat):
    """<season> show next f1 race.
    If the season is not specified or the year
    is wrong the current season will be used"""
    url = "https://api.openf1.org/v1/meetings?year=" + str(datetime.datetime.now().year)

    r = requests.get(url)

    data = r.json()
    r = len(data)
    data = fastf1.get_session(datetime.datetime.now().year, r + 1, 1)


    chat.send((
                  "*{}*\n*Round: *{}\n*Data: *{}\n"
              ).format(str.upper(" ".join(str(data).split()[4:8])),
                       r + 1,
                       str(data.date.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.tzlocal()))),
              syntax="markdown")



@bot.callback("driverstand")
def driverstand(message, data):
    url = "http://ergast.com/api/f1/{}/driverStandings.json".format(data)

    r = requests.get(url)

    data_requests = r.json()['MRData']['StandingsTable']
    data_requests = data_requests['StandingsLists'][0]['DriverStandings']


    text = "Driver | Car | points\n\n"

    for driver in data_requests:
        text += driver['Driver']['code'] + " | " + \
                driver['Constructors'][0]['name'] + " | " + \
                driver['points'] + "\n"

    message.edit(text)


@bot.command("standings")
def standings_command(chat, args):
    """<season> Driver and Constructor standings.
     If the season is not specified or the year is
     wrong the current season will be used"""
    btns = botogram.Buttons()

    if len(args) > 0:
        season = control_year(args[0])
    else:
        season = "current"

    btns[0].callback("Driver Standings", "driverstand", season)

    chat.send("Standings\n*Season: *{}".format(season),
              syntax="markdown", attach=btns)


@bot.callback("menudrivers")
def menudriver(message, data):
    url = "http://ergast.com/api/f1/{}/drivers.json".format(data)

    btns = botogram.Buttons()

    r = requests.get(url)

    data_requests = r.json()['MRData']['DriverTable']['Drivers']

    i = 1
    y = 0

    for driver in data_requests:
        btns[y].callback(driver['familyName'], "drivers",
                         driver['driverId'] + "#{}".format(data))
        if i % 3 == 0:
            y += 1
        i += 1

    message.edit("Drivers\n*Season: *{}".format(data), attach=btns)


@bot.command("drivers")
def drivers_command(chat):
    """list of F1 drivers for current season with Wikipedia links"""

    urls = "https://api.openf1.org/v2/drivers?session_key=latest"

    btns = botogram.Buttons()

    r = requests.get(urls)

    data = r.json()

    i = 1
    y = 0
    for driver in data:
        btns[y].url(driver['full_name'],
                         "https://wikipedia.org/wiki/"+driver['first_name']+"_"+driver['last_name'])
        if i % 3 == 0:
            y += 1
        i += 1

    chat.send("Drivers\n*Season: *{}".format(datetime.datetime.now().year), attach=btns)


@bot.command("constructors")
def constructors_command(chat, args):
    """<season> List F1 constructors
        If the season is not specified or the year is
        wrong the current season will be used"""
    if len(args) > 0:
        season = control_year(args[0])
    else:
        season = "current"

    url = "http://ergast.com/api/f1/{}/constructors.json".format(season)

    btns = botogram.Buttons()

    r = requests.get(url)

    data = r.json()['MRData']['ConstructorTable']['Constructors']

    i = 1
    y = 0

    for driver in data:
        btns[y].callback(driver['name'], "constructorlst",
                         driver['constructorId'] + "#{}".format(season))
        if i % 2 == 0:
            y += 1
        i += 1

    chat.send("Constructors list\n*Season: *{}".format(season), attach=btns)


@bot.command("raceresults")
def raceresults_command(chat, args):
    """<season> List all f1 races and get all results
        If the season is not specified or the year is
        wrong the current season will be used"""
    url = "https://api.openf1.org/v1/meetings?year=" + str(datetime.datetime.now().year)
    r = requests.get(url)

    data = r.json()
    r = len(data)

    if len(args) > 0:
        season = int(args[0])
        num = int(args[1])
    else:
        num = r
        season = datetime.datetime.now().year

    data = fastf1.get_session(season, num, 5)

    data.load()
    data = data.results[['Abbreviation','TeamName','Time','Points']]


    chat.send("Race results\n*Season: *{}".format(season) + "\n" + tabulate(data, headers='keys', showindex=False, tablefmt='psql'), 
              syntax="markdown")


@bot.command("Calendar")
def Calendar_command(chat, args):
    """Calendar of F1 meetings with time in localtime"""

    if len(args) > 0:
        season = int(args[0])
    else:
        season = 2024

    data = str()

    for i in range(1, 25):
        try:
            data += str(fastf1.get_session(season, i, 5)) + ' ' + \
            str(fastf1.get_session(season, i, 5).date.replace(tzinfo=tz.gettz('UTC')
).astimezone(tz.tzlocal())) + '\n\n'
        except ValueError as e:
            break

    chat.send("Calendar\n*Season: *{}\n".format(season)+data)

@bot.command("Trackdata")
def Trackdata_command(chat, args):
    """Print track speed telemetry"""

    if len(args) <= 0:
        chat.send("Input data in format /Track_data <year weekend driver>\n")
    print(args)
    
    plotting(args)
    name = "/Users/veronikamorozova/{}".format(args[0])+"_"+"{}".format(args[1])+"_"+"{}".format(args[2])+".png"
    chat.send_photo(name)




if __name__ == "__main__":
    bot.run()
