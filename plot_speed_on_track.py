"""Module for plotting track"""
import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection

import fastf1 as ff1

def plotting(args):
    """ Function that takes args (which contains <year, weekend, driver>)
        and plot the heatmap on f1 track with info about speead during the race"""
    year = int(args[0])
    wknd = int(args[1])
    ses = 'R'
    driver = args[2]
    colormap = mpl.cm.plasma

    session = ff1.get_session(year, wknd, ses)
    weekend = session.event
    session.load()
    lap = session.laps.pick_driver(driver).pick_fastest()

    x = lap.telemetry['X']              
    y = lap.telemetry['Y']              
    color = lap.telemetry['Speed']      

    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)

    fig, ax = plt.subplots(sharex=True, sharey=True, figsize=(12, 6.75))
    fig.suptitle(f'{weekend.name} {year} - {driver} - Speed', size=24, y=0.97)

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.12)
    ax.axis('off')

    ax.plot(lap.telemetry['X'], lap.telemetry['Y'],
    color='black', linestyle='-', linewidth=16, zorder=0)

    norm = plt.Normalize(color.min(), color.max())
    lc = LineCollection(segments, cmap=colormap, norm=norm,
        linestyle='-', linewidth=5)

    lc.set_array(color)

    line = ax.add_collection(lc)

    cbaxes = fig.add_axes([0.25, 0.05, 0.5, 0.05])
    normlegend = mpl.colors.Normalize(vmin=color.min(), vmax=color.max())
    legend = mpl.colorbar.ColorbarBase(cbaxes, norm=normlegend, cmap=colormap,
                       orientation="horizontal")

    name = "{}".format(year)+"_"+"{}".format(wknd)+"_"+"{}".format(driver)+".png"

    plt.savefig(name)
